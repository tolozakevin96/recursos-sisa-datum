package com.auraportaldoc;

import java.io.IOException;
import java.io.StringReader;
import java.security.Security;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.tempuri.AuraPortalDocLocator;
import org.tempuri.AuraPortalDocSoap;
import org.tempuri.AuraPortalDocSoapStub;

import com.auraportaldoc.AuraPortalException;
import com.ibm.websphere.webservices.Constants;
import com.sisa.model.clase.AuraPortal_DocumentLibrary;
import com.sisa.security.SISASSLSocketFactory;


public class AuraPortalDoc {

	private AuraPortalDocLocator locator = null;
	private AuraPortalDocSoap service;
	private AuraPortalDocSoapStub stub;
	
	public AuraPortalDoc() throws ServiceException, IOException, UnsupportedOperationException, SOAPException{
		
		Security.setProperty("ssl.SocketFactory.provider", SISASSLSocketFactory.class.getCanonicalName());

		locator = new AuraPortalDocLocator();
		service = locator.getAuraPortalDocSoap();
		stub = (AuraPortalDocSoapStub)service;
		stub.setUsername("ap5");
		stub.setPassword("!Q2w#E4r");
		
		//***PRD***
		/*HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("UserData","pUbIgwcu1yU=");
		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NZIEukjAKixMVSWsLfl2HIgXQiRTTbcStapxaXd/CUeslMurR4UQYvagAKxdf1Arq7IyVAzGQTuhC9Dr3lE3yzryZoaRxcD3qcEX/TgsAfK0");
		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);*/

		//***UAT
//		/*HashMap<String, String> headers = new HashMap<String, String>();
//		headers.put("UserData","pUbIgwcu1yU=");
//		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NflYKThz3dsywrlxFYnjEv0zbV1MQHBYfmmCKlnY+CwB86b8+aVs/b4qMUgltrCK7E0TwgAYD/B7chr4Ps9D1ps+mC1a3P9SUaMJ0ZI2zf3Z");
//		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);*/		

		//***DEV***
//		HashMap<String, String> headers = new HashMap<String, String>();
//		headers.put("UserData","pUbIgwcu1yU=");
//		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NVGpXdQvSC954topdg7Rn4+FhnHVwHDMoEbEXmzXEgN/8snGTklY4AEB9sHKnrYioTzLOeSqmvx3oPAI3Q2hEIgigWDMI48znCntCCRhcZky");
//		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);

        
	}
	
	public String obtenerTokenDoc (String nombreBiblioteca, String tipoProceso, String numRefProceso) throws AuraPortalException {		
		
		JAXBContext jaxbContext;
		String tokenDoc;
		String xmlData;
		
		try{
			
			xmlData =	"<![CDATA[\r\n"
						+ "<AuraPortal_DocumentLibrary version=\"4.2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"getDocDictionaryLibraryList.xsd\">\r\n"
						+ "<Library_Name>" + nombreBiblioteca + "</Library_Name>\r\n"
						+ "<Integration_Filter>\r\n"
						+ "<Type>" + tipoProceso + "</Type>\r\n"
						+ "<Element>" + numRefProceso + "</Element>\r\n"
						+ "</Integration_Filter>\r\n"
						+ "</AuraPortal_DocumentLibrary>\r\n"
						+ "]]>";
			
			String xmlDataResponse = service.getDocDictionaryLibraryList(xmlData);
			
			xmlDataResponse = xmlDataResponse.substring(1, 2) + xmlDataResponse.substring(2);
			System.out.println(xmlDataResponse);
	
			jaxbContext = JAXBContext.newInstance(AuraPortal_DocumentLibrary.class);              
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    AuraPortal_DocumentLibrary token = (AuraPortal_DocumentLibrary) jaxbUnmarshaller.unmarshal(new StringReader(xmlDataResponse));
		    tokenDoc = token.getDocumentos().getToken();
			
		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
	
		return tokenDoc;
	}
	
	public String obtenerBase64Doc (String TokenDocumento) throws AuraPortalException {		
		
		JAXBContext jaxbContext;
		String docBase64;
		String xmlData;
		
		try{
			
			xmlData =	"<![CDATA[\r\n"
						+ "<AuraPortal_DocumentLibrary version=\"4.2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"getDocDictionaryLibrary.xsd\">\r\n"
						+ "<Document_Token>" + TokenDocumento + "</Document_Token>\r\n"
						+ "</AuraPortal_DocumentLibrary>\r\n"
						+ "]]>";
			
			String xmlDataResponse = service.getDocDictionaryLibrary(xmlData);
			//System.out.println(xmlDataResponse);

			xmlDataResponse = xmlDataResponse.substring(1, 2) + xmlDataResponse.substring(2);

			jaxbContext = JAXBContext.newInstance(AuraPortal_DocumentLibrary.class);              
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    AuraPortal_DocumentLibrary base64Doc = (AuraPortal_DocumentLibrary) jaxbUnmarshaller.unmarshal(new StringReader(xmlDataResponse));
		    docBase64 = base64Doc.getDocumentos().getContent();

		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
	
		return docBase64;
	}
	
	public String enviarDocBiblioteca (String nombreBiblioteca, String tipoProceso, String numRefProceso, String nombreDocument, String descripDocument, String documetBase64) throws AuraPortalException {		
		
		String respuesta;
		String xmlData;
		
		try{

			xmlData =	"<![CDATA[\r\n"
					+ "<AuraPortal_DocumentLibrary version=\"4.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"uploadDocumentToDictionaryLibrary.xsd\">\r\n"
					+ "<Library_Name>" + nombreBiblioteca + "</Library_Name>\r\n"
					+ "<Integration_Target>\r\n"
					+ "<Type>" + tipoProceso + "</Type>\r\n"
					+ "<Element>" + numRefProceso + "</Element>\r\n"
					+ "</Integration_Target>\r\n"
					+ "<Document_Name>" + nombreDocument + "</Document_Name>\r\n"
					+ "<Document_Description>" + descripDocument + "</Document_Description>\r\n"
					+ "<Content>" + documetBase64 + "</Content>\r\n"
					+ "</AuraPortal_DocumentLibrary>\r\n"
					+ "]]>";
			
			String xmlDataResponse = service.uploadDocumentToDictionaryLibrary_Unique(xmlData);
			respuesta = xmlDataResponse;
			System.out.println(xmlDataResponse);

			
		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
	
		return respuesta;
	}	
    
}	