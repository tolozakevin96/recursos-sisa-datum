package com.auraportal;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPException;

import org.tempuri.AuraPortalProcesosLocator;
import org.tempuri.AuraPortalProcesosSoap;
import org.tempuri.AuraPortalProcesosSoapStub;
import org.tempuri.PanelDatosVerGCResult;
import org.w3c.dom.NodeList;

import com.ibm.websphere.webservices.Constants;
import com.ibm.ws.webservices.engine.xmlsoap.SOAPElement;
import com.sisa.model.GCBeneficiario;
import com.sisa.model.GCComision;
import com.sisa.model.GCDeclaracion;
import com.sisa.model.GCDireccion;
//import com.sisa.model.GCPlacasVeh;
import com.sisa.model.GCTelefono;
import com.sisa.model.GCEstadistica;
import com.sisa.model.GCDependiente;

public class AuraPortalProcesos {

	private AuraPortalProcesosLocator locator = null;
	private AuraPortalProcesosSoap service;
	private AuraPortalProcesosSoapStub stub;
	
	public AuraPortalProcesos() throws ServiceException, IOException, UnsupportedOperationException, SOAPException{
		locator = new AuraPortalProcesosLocator();
		service = locator.getAuraPortalProcesosSoap();
		stub = (AuraPortalProcesosSoapStub)service;
		stub.setUsername("ap5");
		stub.setPassword("!Q2w#E4r");
		
		//***PRD***
		/*HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("UserData","pUbIgwcu1yU=");
		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NbDpP+JZfQ32B0tRBTbKJ5JqszbmTaR4zp/1CogJruoSAIqNCcJarFlr+cSHgAEX4ujjpKmwrnMAc9ib487nZgOpRFLCJY9vR7xC6vOlYhBu");
		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);*/

		//***UAT
//		/*HashMap<String, String> headers = new HashMap<String, String>();
//		headers.put("UserData","pUbIgwcu1yU=");
//		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NVGpXdQvSC954topdg7Rn4+FhnHVwHDMoEbEXmzXEgN/8snGTklY4AEB9sHKnrYioTzLOeSqmvx3RRduVsZ2Qz7gKFJNk1u0FJYaHTJLestV");
//		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);*/	

		//***DEV***
//		HashMap<String, String> headers = new HashMap<String, String>();
//		headers.put("UserData","pUbIgwcu1yU=");
//		headers.put("ApiKey","I3+JCWqSTinaPGTPOyA7XbcqTSaMOa7mdr2yPt1y6BEWh8javfNknsyXZrsAoKciJlDwd3pacvNpkO0Ti2l7NbDpP+JZfQ32B0tRBTbKJ5JqszbmTaR4zp/1CogJruoSAIqNCcJarFlr+cSHgAEX4ujjpKmwrnMA1+2/tZchoo37QFdYOqreTW/cJseGdEZF");
//		stub._setProperty(Constants.REQUEST_TRANSPORT_PROPERTIES, headers);
		
		
		try {
	
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, obtenerCertTrusted(), new java.security.SecureRandom());
            HttpsURLConnection.setDefaultHostnameVerifier(VerificarHost());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        
	}
	
	public List<GCDireccion> obtenerDirecciones(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCDireccion> gcDirecciones = new ArrayList<GCDireccion>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos,"");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			
			
			Node panelDatosVerGC = (Node) element.getFirstChild();
			NodeList direcciones = panelDatosVerGC.getChildNodes();
			
			for(int i = 0; i< direcciones.getLength(); i++){
				Node nodeDireccion = (Node) direcciones.item(i);
				if(nodeDireccion!=null){
					GCDireccion direccion = new GCDireccion();
					NodeList propiedades = nodeDireccion.getChildNodes();
					String tmpVal = "";
					String nombre = "";
					for(int n = 0; n < propiedades.getLength(); n++){
						Node propiedad = (Node) propiedades.item(n);
						nombre = propiedad.getNodeName();
						nombre = nombre.replace("_", "").replace("x0020", " ").replace("x0031", "").replace("x0033", "");
						tmpVal = propiedad.getValue();
						System.out.println(nombre);
						System.out.println(nombre+"\t"+tmpVal);
						if(tmpVal !=null){
							if(!tmpVal.trim().equals("")){
								if(tmpVal.indexOf("###") >= 0){
									tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
								}
								if(tmpVal.indexOf("(") >= 0){
									tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
								}
								if(nombre.equals("GCTipo de Direccion") || nombre.equals("RepTipo de Direccion") ){
									direccion.setTipoDirec(tmpVal);
								}
								if(nombre.equals("GCPais") || nombre.equals("GCRepPais")){
									direccion.setPais(tmpVal);
								}
								if(nombre.equals("GCEstado") || nombre.equals("GCRepEstado")){
									direccion.setEstado(tmpVal);
								}
								if(nombre.equals("GCCiudad") || nombre.equals("GCRepCiudad")){
									direccion.setCiudad(tmpVal);
								}
								if(nombre.equals("GCMunicipio") || nombre.equals("RepMunicipio")){
									direccion.setMunicipio(tmpVal);
								}
								if(nombre.equals("Nombre de la Calle") || nombre.equals("Repnombre de la calle")){
									direccion.setDirCalle(tmpVal);
								}
								if(nombre.equals("Nombre de la Colonia") || nombre.equals("Repnombre de la colonia")){
									direccion.setDirColonia(tmpVal);
								}
								if(nombre.equals("Numero de Casa") || nombre.equals("Repn�mero de casa")){
									direccion.setDirNumero(tmpVal);
								}
								if(nombre.equals("GCCorrelativoDirecciones") || nombre.equals("GCCorrelativoDireccionesRP")){
									direccion.setNumCorrDirec(Integer.parseInt(tmpVal));
								}
								if(nombre.equals("Direccion") || nombre.equals("RepDireccion2")){
									direccion.setDireccion(tmpVal);
								}
								if(nombre.equals("Direccion principal") || nombre.equals("RepDireccion Principal")){
									if(tmpVal.equals("True")){
										direccion.setDireccionPrincipal(true);
									}else{
										direccion.setDireccionPrincipal(false);
									}
								}
							}
						}
					}
					if(direccion.getTipoDirec()!=null){
						gcDirecciones.add(direccion);
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcDirecciones;
	}
	
	public List<GCTelefono> obtenerTelefonos(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCTelefono> gcTelefonos = new ArrayList<GCTelefono>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			
			
			Node panelDatosVerGC = (Node) element.getFirstChild();
			NodeList nodeListTelefonos = panelDatosVerGC.getChildNodes();
			
			for(int i = 0; i< nodeListTelefonos.getLength(); i++){
				Node nodeTelefono = (Node) nodeListTelefonos.item(i);
				if(nodeTelefono!=null){
					GCTelefono telefono = new GCTelefono();
					NodeList propiedades = nodeTelefono.getChildNodes();
					String tmpVal = "";
					String nombre = "";
					for(int n = 0; n < propiedades.getLength(); n++){
						Node propiedad = (Node) propiedades.item(n);
						nombre = propiedad.getNodeName();
						nombre = nombre.replace("_", "").replace("x0020", " ").replace("x0031", "").replace("x0033", "");
						tmpVal = propiedad.getValue();
						if(tmpVal !=null){
							if(!tmpVal.trim().equals("")){
								if(tmpVal.indexOf("###") >= 0){
									tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
								}
								if(tmpVal.indexOf("(") >= 0){
									tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
								}
								if(nombre.equals("GCCorrelativoTelefono") || nombre.equals("GCCorrelativoTelefonoRP")){
									telefono.setNumCorrTel(Integer.parseInt(tmpVal));
								}
								if(nombre.equals("Tipo de Tel�fono") || nombre.equals("RepTipo de Telefono")){
									telefono.setTipoTelef(tmpVal);
								}
								if(nombre.equals("Numero de Telefono") || nombre.equals("RepNumero de Telefono")){
									telefono.setNumTelef(tmpVal);
								}
								if(nombre.equals("Numero de Extension") || nombre.equals("RepNumero de Extension")){
									telefono.setNumExt(Integer.parseInt(tmpVal));
								}
								if(nombre.equals("Es Telefono Principal") || nombre.equals("RepTelefono Principal")){
									if(tmpVal.equals("True")){
										telefono.setTelPpal(true);
									} else {
										telefono.setTelPpal(false);
									}
								}
							}
						}
					}
					gcTelefonos.add(telefono);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcTelefonos;
	}

	
	public List<GCEstadistica> obtenerEstadisticas(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCEstadistica> gcEstadisticas = new ArrayList<GCEstadistica>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);

			
			Node panelDatosVerGC = (Node) element.getFirstChild();
			NodeList nodeListEstadisticas = panelDatosVerGC.getChildNodes();
			
			for(int i = 0; i< nodeListEstadisticas.getLength(); i++){
				Node nodeEstadistica = (Node) nodeListEstadisticas.item(i);
				if(nodeEstadistica!=null){
					GCEstadistica estadistica = new GCEstadistica();
					NodeList propiedades = nodeEstadistica.getChildNodes();
					String tmpVal = "";
					String nombre = "";
					for(int n = 0; n < propiedades.getLength(); n++){
						Node propiedad = (Node) propiedades.item(n);
						nombre = propiedad.getNodeName();
						nombre = nombre.replace("_", "").replace("x0020", " ").replace("x002F"," ").replace("x0031", "").replace("x0032", "").replace("x0033", "");
						tmpVal = propiedad.getValue();
						System.out.println(nombre+"\t"+tmpVal);
						
						if(tmpVal !=null){
							if(!tmpVal.trim().equals("")){
								if(tmpVal.indexOf("###") >= 0){
									tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
								}
								if(tmpVal.indexOf("(") >= 0){
									tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
								}
								
								if(nombre.equals("PREESTADISTICAS TXTDESCESTADISTICA")){
									estadistica.setCodEst(tmpVal);
								}
								if(nombre.equals("PREESTADISTICAS TXTCODESTAD")){
									estadistica.setValEst(tmpVal);
								}
								if(nombre.equals("TXTCODRAMO")){
									estadistica.setCodRamoCert(tmpVal);
								}

							}
						}
					}
					
					gcEstadisticas.add(estadistica);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcEstadisticas;
	}
	
	public List<GCDependiente> obtenerDependientes(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCDependiente> gcDependientes = new ArrayList<GCDependiente>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			if (element !=null && element.getLength()!= 0){
			
				Node panelDatosVerGC = (Node) element.getFirstChild();
				NodeList nodeListDependientes = panelDatosVerGC.getChildNodes();
				
				for(int i = 0; i< nodeListDependientes.getLength(); i++){
					Node nodeDependiente = (Node) nodeListDependientes.item(i);
					if(nodeDependiente!=null){
						GCDependiente dependiente = new GCDependiente();
						NodeList propiedades = nodeDependiente.getChildNodes();
						String tmpVal = "";
						String nombre = "";
						for(int n = 0; n < propiedades.getLength(); n++){
							Node propiedad = (Node) propiedades.item(n);
							nombre = propiedad.getNodeName();
							nombre = nombre.replace("_", "").replace("x0020", " ").replace("x002F"," ").replace("x0031", "").replace("x0032", "").replace("x0033", "");
							tmpVal = propiedad.getValue();
							System.out.println(nombre+"\t"+tmpVal);
							
							if(tmpVal !=null){
								if(!tmpVal.trim().equals("")){
									if(tmpVal.indexOf("###") >= 0){
										tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
									}
									if(tmpVal.indexOf("(") >= 0){
										tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
									}
									
									if(nombre.equals("TXTCODIGODEPENDIENTE")){
										dependiente.setCodCli(tmpVal);
									}
									if(nombre.equals("PREPARENTESCO Codigo")){
										dependiente.setCodParent(tmpVal);
									}
									//
									if(nombre.equals("TXTINDRECADCTO")) {
										dependiente.setIndRecaDcto(tmpVal);
									}
									
									if(nombre.equals("TXTTIPORECADCTO")) {
										dependiente.setTipoRecaDcto(tmpVal);
									}
									
									if(nombre.equals("TXTCODRECADCTO")) {
										dependiente.setCodRecaDcto(tmpVal);
									}
									
									//
	
								}
							}
						}
						
						gcDependientes.add(dependiente);
					}
				}
			
			}

		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcDependientes;
	}
	
	public List<GCBeneficiario> obtenerBeneficiarios(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCBeneficiario> gcBeneficiarios = new ArrayList<GCBeneficiario>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			if (element !=null && element.getLength()!= 0){
			
				Node panelDatosVerGC = (Node) element.getFirstChild();
				NodeList nodeListBeneficiarios = panelDatosVerGC.getChildNodes();
				
				for(int i = 0; i< nodeListBeneficiarios.getLength(); i++){
					Node nodeBeneficiario = (Node) nodeListBeneficiarios.item(i);
					if(nodeBeneficiario!=null){
						GCBeneficiario beneficiario = new GCBeneficiario();
						NodeList propiedades = nodeBeneficiario.getChildNodes();
						String tmpVal = "";
						String nombre = "";
						for(int n = 0; n < propiedades.getLength(); n++){
							Node propiedad = (Node) propiedades.item(n);
							nombre = propiedad.getNodeName();
							nombre = nombre.replace("_", "").replace("x0020", " ").replace("x002F"," ").replace("x0031", "").replace("x0032", "").replace("x0033", "");
							tmpVal = propiedad.getValue();
							System.out.println(nombre+"\t"+tmpVal);
							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							
							if(tmpVal !=null){
								if(!tmpVal.trim().equals("")){
									if(tmpVal.indexOf("###") >= 0){
										tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
									}
									if(tmpVal.indexOf("(") >= 0){
										tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
									}
									
									if(nombre.equals("TXTGCBENEFICIARIONOMBRES")){
										beneficiario.setNomBen(tmpVal);
									}
									if(nombre.equals("TXTGCAPELLIDOSBENEFICIARIO")){
										beneficiario.setApeBen(tmpVal);
									}
									if(nombre.equals("DECGCPORCENTAJEBENEFICIARIO")){
										beneficiario.setPorcPart(new BigDecimal(tmpVal));
									}
									if(nombre.equals("DTGCFECHANACBENEFICIARIO")){
										beneficiario.setFecNac(df.parse(tmpVal));
									}
									if(nombre.equals("PREGCPARENTESCOBENEFICIARIO Codigo")){
										beneficiario.setCodParent(tmpVal);
									}
									if(nombre.equals("PREGCPARENTESCOBENEFICIARIO SSTIPOBENEFICIARIO")){
										beneficiario.setTipoBen(tmpVal);
									}
									if(nombre.equals("TXTGCSTSBEN")){
										beneficiario.setStsBen(tmpVal);
									}
										
								}
							}
						}
						
						gcBeneficiarios.add(beneficiario);
					}
				}
			
			}

		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcBeneficiarios;
	}
	
	/*
	public List<GCPlacasVeh> obtenerPlacas(String numRefProceso, String nombreGrupoCampo) throws RemoteException{
		List<GCPlacasVeh> gcPlacas = new ArrayList<GCPlacasVeh>();
		//try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampo, "");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			Node panelDatosVerGC = (Node) element.getFirstChild();
			NodeList nodeListPlacas = panelDatosVerGC.getChildNodes();
			
			for(int i = 0; i< nodeListPlacas.getLength(); i++){
				Node nodePlaca = (Node) nodeListPlacas.item(i);
				if(nodePlaca!=null){
					GCPlacasVeh placa = new GCPlacasVeh();
					NodeList propiedades = nodePlaca.getChildNodes();
					String tmpVal = "";
					String nombre = "";
					for(int n = 0; n < propiedades.getLength(); n++){
						Node propiedad = (Node) propiedades.item(n);
						nombre = propiedad.getNodeName();
						nombre = nombre.replace("_", "").replace("x0020", " ").replace("x0031", "").replace("x0033", "");
						tmpVal = propiedad.getValue();
						
						if(tmpVal !=null){
							if(!tmpVal.trim().equals("")){
								if(tmpVal.indexOf("###") >= 0){
									tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
								}
								if(tmpVal.indexOf("(") >= 0){
									tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
								}
								
								if(nombre.equals("GCTXTPLACA")){
									placa.setNumPlaca(tmpVal);
								}
							}
						}
						
					}
					gcPlacas.add(placa);
				}
			}
		/*} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return gcPlacas;
	}
	*/
	
	public List<GCComision> obtenerComisiones(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCComision> gcComision = new ArrayList<GCComision>();
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			if (element !=null && element.getLength()!= 0){
			
				Node panelDatosVerGC = (Node) element.getFirstChild();
				NodeList nodeListComisionesEspeciales = panelDatosVerGC.getChildNodes();
				
				for(int i = 0; i< nodeListComisionesEspeciales.getLength(); i++){
					Node nodeComisionEspecial = (Node) nodeListComisionesEspeciales.item(i);
					if(nodeComisionEspecial!=null){
						GCComision comision = new GCComision();
						NodeList propiedades = nodeComisionEspecial.getChildNodes();
						String tmpVal = "";
						String nombre = "";
						for(int n = 0; n < propiedades.getLength(); n++){
							Node propiedad = (Node) propiedades.item(n);
							nombre = propiedad.getNodeName();
							nombre = nombre.replace("_", "").replace("x0020", " ").replace("x002F"," ").replace("x0031", "").replace("x0032", "").replace("x0033", "");
							tmpVal = propiedad.getValue();
							System.out.println(nombre+"\t"+tmpVal);
							
							if(tmpVal !=null){
								if(!tmpVal.trim().equals("")){
									if(tmpVal.indexOf("###") >= 0){
										tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
									}
									if(tmpVal.indexOf("(") >= 0){
										tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
									}
									
									if(nombre.equals("TXTGCCLIENTE CE")){//Cliente
										comision.setCliente(tmpVal);
									}
									if(nombre.equals("ENTGCCOD CLIENTE CE")){//CodigoCliente
										comision.setCodCli(tmpVal);
									}
									
									if(nombre.equals("TXTGCPRODUCTO CE")){//Producto
										comision.setCodProd(tmpVal);
									}
									if(nombre.equals("ENTGCNUM POLIZA CE")){//NumPol
										comision.setNumPol(tmpVal);
									}
									if(nombre.equals("TXTGCCE INICIAL")){//Inicial
										comision.setInicial(tmpVal);
									}
									if(nombre.equals("TXTGCCE RENOVACI�N")){//Renovacion
										comision.setRenovacion(tmpVal);
									}
	
								}
							}
						}
						
						gcComision.add(comision);
					}
				}
			
			}

		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcComision;
	}
	
	public List<GCDeclaracion> obtenerDeclaraciones(String numRefProceso, String nombreGrupoCampos) throws AuraPortalException{
		List<GCDeclaracion> gcDeclaracion = new ArrayList<GCDeclaracion>();
		System.out.println("numRefProceso " +numRefProceso);
		System.out.println("nombreGrupoCampos " +nombreGrupoCampos);
		try{
			PanelDatosVerGCResult panelDatosResult = service.panelDatosVerGC(numRefProceso, nombreGrupoCampos, "2");
			SOAPElement element = (SOAPElement) panelDatosResult.get_any();
			
			System.out.println(element);
			if (element !=null && element.getLength()!= 0){
			
				Node panelDatosVerGC = (Node) element.getFirstChild();
				NodeList nodeListDeclaraciones = panelDatosVerGC.getChildNodes();
				
				for(int i = 0; i< nodeListDeclaraciones.getLength(); i++){
					Node nodeDeclaracion = (Node) nodeListDeclaraciones.item(i);
					if(nodeDeclaracion!=null){
						GCDeclaracion declaracion = new GCDeclaracion();
						NodeList propiedades = nodeDeclaracion.getChildNodes();
						String tmpVal = "";
						String nombre = "";
						for(int n = 0; n < propiedades.getLength(); n++){
							Node propiedad = (Node) propiedades.item(n);
							nombre = propiedad.getNodeName();
							nombre = nombre.replace("_", "").replace("x0020", " ").replace("x002F"," ").replace("x0031", "").replace("x0032", "").replace("x0033", "");
							tmpVal = propiedad.getValue();
							System.out.println(nombre+"\t"+tmpVal);
							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							if(tmpVal !=null){
								if(!tmpVal.trim().equals("")){
									if(tmpVal.indexOf("###") >= 0){
										tmpVal = tmpVal.substring(tmpVal.indexOf("###")+3);
									}
									if(tmpVal.indexOf("(") >= 0){
										tmpVal = tmpVal.substring(0, tmpVal.indexOf("("));
									}
									
									if(nombre.equals("FechaDec")){
										declaracion.setFecDeclar(df.parse(tmpVal));//stsDeclar
									}
									if(nombre.equals("Sts")){
										declaracion.setStsDeclar(tmpVal);//stsDeclar
									}
									if(nombre.equals("SumaDec")){ 
										declaracion.setMtoSumaDeclar(new BigDecimal(tmpVal));//mtoSumaDeclar
									}
									if(nombre.equals("IntervDeclara")){
										declaracion.setIntervDeclar(tmpVal);//intervDeclar
									}
									if(nombre.equals("porcBasePrimInic")){ 
										declaracion.setPorcBasePrimInic(new BigDecimal(tmpVal));//porcBasePrimInic
									}
									if(nombre.equals("mtoPrimaInicMoneda")){ 
										declaracion.setMtoPrimaInicMoneda(new BigDecimal(tmpVal));//mtoPrimaInicMoneda
									}
									if(nombre.equals("porcBasePrimMin")){ 
										declaracion.setPorcBasePrimMin(new BigDecimal(tmpVal));//porcBasePrimMin
									}
									if(nombre.equals("mtoPrimaMinimaMoneda")){ 
										declaracion.setMtoPrimaMinimaMoneda(new BigDecimal(tmpVal));//mtoPrimaMinimaMoneda
									}
									if(nombre.equals("numCert")){
										declaracion.setNumCert(Integer.parseInt(tmpVal));//numCert
									}
									if(nombre.equals("descCert")){
										declaracion.setDescCert(tmpVal);//descCert
									}
									if(nombre.equals("claseBien")){
										declaracion.setClaseBien(tmpVal);//claseBien
									}
									if(nombre.equals("descClaseBien")){
										declaracion.setDescClaseBien(tmpVal);//descClaseBien
									}
									if(nombre.equals("codBien")){
										declaracion.setCodBien(tmpVal);//codBien
									}
									if(nombre.equals("descBien")){
										declaracion.setDescBien(tmpVal);//descBien
									}
									if(nombre.equals("colonia bien")){
										declaracion.setColonia(tmpVal);//colonia
									}
									if(nombre.equals("calle bien")){
										declaracion.setCalle(tmpVal);//calle
									}
									if(nombre.equals("numero bien")){
										declaracion.setNumero(tmpVal);//numero
									}
									if(nombre.equals("direcci�n bien")){
										declaracion.setDireccion(tmpVal);//direccion
									}
									if(nombre.equals("NumDetDeclar")){
										declaracion.setNumDetDeclar(tmpVal);//numDetDeclar
									}
									if(nombre.equals("ENTNumDetDeclar")){
										declaracion.seteNumDetDeclar(Integer.parseInt(tmpVal));//numDetDeclar
									}
	
								}
							}
						}
						
						gcDeclaracion.add(declaracion);
					}
				}
			
			}

		}catch (Exception e){
			e.printStackTrace();
			throw new AuraPortalException(e);
		}
		
		return gcDeclaracion;
	}//fin
	
    public TrustManager[] obtenerCertTrusted() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };
        return trustAllCerts;
    }

    public HostnameVerifier VerificarHost() {
        // Create all-trusting host name verifier
        HostnameVerifier HostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        return HostsValid;
    }
}
