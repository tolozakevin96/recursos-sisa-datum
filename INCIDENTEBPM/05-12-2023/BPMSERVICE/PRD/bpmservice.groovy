/****Inicio: variables Ambiente****/
def jiraKey
def pathJsonJira
def pathJsonBitBucket
def pathResponseBucket
def comandAnsible
def response
def responseJira
def responseToken
def responseBitBucket
def sitioweb
def pathBPMService
def responseAnsible
def ansibleResponse
def hostNameServer
def errorCode = 200
def msgErr
def hostnameCob
/****Fin: variables Ambiente****/


/****Inicio: variables Atlassian****/
def urlAtlassian
def sitio
def errorPreparing
def inBuilding 
def deploy 
def testing 
def review
def usuarioAtlassian
def usuarioBitBucket
def commentJira
def errorMesaje
/****Fin: variables Atlassian****/

node('jenkins_slave01') {
    stage('Preparing'){
        echo '/*************Inicio de Metodo Preparing*************************/'
        
        /****Inicio: variables Ambiente****/
        jiraKey = idJira
        urlAtlassian = env.URL_REST_ATLASSIAN+jiraKey
        pathJsonJira = WORKSPACE+'/jsonTaskJira.json'
        pathJsonBitBucket = WORKSPACE+'/jsonTokenBitbucket.json'
        pathResponseBucket = WORKSPACE+'/jsonResposeBitbucket.json'
        usuarioAtlassian = 'atlassian'
        usuarioBitBucket = 'bitbucket-pull-request'      
        pathBPMService = env.PATH_PRD_BPMSERVICE
        ansiblePath = env.ANSIBLE_BPMSERVICE
        hostnameCob = env.HOSTNAME_BPMSERVICE_COB        
        /****Fin: variables Ambiente****/
        
        /****Inicio: variables Jira****/
        sitio=env.SITE_ATLASSIAN  
        inBuilding = [transition: [id: '321']]
        errorPreparing = [transition: [id: '401']]
        deploy = [transition: [id: '331' ]]
        errorBuild = [transition: [id: '411']]
        deployCob = [transition: [id: '341' ]]
        errorDeploy = [transition: [id: '421']]      
        review = [transition: [id: '361']]
        /****Fin: variables Jira****/
        
        commentJira =[body: 'Se Inicio el ciclo CI/CD en Jenkins']        
        jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
        jiraTransitionIssue idOrKey: jiraKey, input: inBuilding, site: sitio 
        
    }
    
    stage('Building') {
        echo '/*************Inicio de Metodo Building*************************/'
        def codeResponse =300
        script{
            try{            
                response = httpRequest authentication: usuarioAtlassian ,url: urlAtlassian, outputFile: pathJsonJira
                codeResponse=response.status
            }catch(Exception){
                codeResponse = 300
            }
            
        }
        
        if ( codeResponse!= 200){
            commentJira =[body: 'Se encontro un error al consultar la tarea en api estatus code: '+response.status]        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio            
            jiraTransitionIssue idOrKey: jiraKey, input: errorBuild, site: sitio            
            sh "exit 1"            
        }else{
            script{
                try{
                    responseJira = readJSON file: pathJsonJira                    
                    try{
                        build propagate: true, job: 'BITBUCKET_AURAPORTALDOC'
                        build propagate: true, job: 'BITBUCKET_AURAPORTALPROCESOS'
                        build propagate: true, job: 'BITBUCKET_BPMSERVICE'
                        build propagate: true, job: 'BITBUCKET_BPMSERVICEEAR'
                    }catch(Exception e){
                        errorMesaje = 'Error en la descarga del repositorio'
                        sh "exit 1"                         
                    }                
                    try{
                        hostNameServer = responseJira.fields.customfield_10055.value
                    }catch(Exception e){
                        errorMesaje = 'Debe de definir un servidor Valido'
                        sh "exit 1"                         
                    }   
                    commentJira =[body: 'Se realizo la extracción de la información de la tarea']        
                    jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                    jiraTransitionIssue idOrKey: jiraKey, input: deploy, site: sitio

                }catch(Exception e){

                    if(errorMesaje==null){
                        errorMesaje=e.toString()
                    }
                    commentJira =[body: 'Ocurrio el siguiente error en la construcción '+errorMesaje]        
                    jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                    jiraTransitionIssue idOrKey: jiraKey, input: errorBuild, site: sitio
                    sh "exit 1" 
                }
            }
             
          
        }
    }

    stage('Deploy'){
        echo '/*************Inicio de Metodo Deploy*************************/'
        comandAnsible = 'ansible-playbook '+ansiblePath
        comandAnsible=comandAnsible+' -e env="prd" -e host='+ '"' + hostNameServer + '"' + ' -e id='+ '"' + jiraKey + '"' +' -e dir='+'"' + pathBPMService + '"'
        ansibleResponse=pathBPMService+'output.json'
        def comando = env.DELETE_SALTO_LINEA+' '+ansibleResponse
        script{
            try{
                sh "rm -f "+ ansibleResponse
                sh comandAnsible
            }catch(Exception){
                errorCode = 504
            }
            try{
                sh comando
                ansibleResponse = readJSON file: ansibleResponse
                msgErr= ansibleResponse.msg
            }catch(Exception){
                errorCode = 504
                msgErr= "ocurrio un error en el despliegue favor validar la construcción"
            }
        }
        
        
        if (errorCode!=200){
            commentJira =[body: 'Se encontro el siguiente error en el despliegue del aplicativo: '+msgErr]        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
            jiraTransitionIssue idOrKey: jiraKey, input: errorDeploy, site: sitio            
            sh "exit 1"

        }else{
            commentJira =[body: 'Se realizo el despliegue del aplicativo indicado']        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
             
        }
        
    }

    stage('COB'){
        echo '/*************Inicio de Metodo COB*************************/'
        if (hostnameCob!="NA"){
            comandAnsible = 'ansible-playbook '+ansiblePath
            comandAnsible=comandAnsible+' -e env="prd" -e host='+ '"' + hostnameCob + '"' + ' -e id='+ '"' + jiraKey + '"' +' -e dir='+'"' + pathBPMService + '"'
            ansibleResponse=pathBPMService+'output.json'
            def comando = env.DELETE_SALTO_LINEA+' '+ansibleResponse
            script{
                try{
                    sh "rm -f "+ ansibleResponse
                    sh comandAnsible
                }catch(Exception){
                    errorCode = 504
                }
                try{
                    sh comando
                    ansibleResponse = readJSON file: ansibleResponse
                    msgErr= ansibleResponse.msg
                }catch(Exception){
                    errorCode = 504
                    msgErr= "ocurrio un error en el despliegue de COB favor validar la construcción"
                }
            }
            
            
            if (errorCode!=200){
                commentJira =[body: 'Se encontro el siguiente error en el despliegue en COB del aplicativo: '+msgErr]        
                jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio

            }else{
                commentJira =[body: 'Se realizo el despliegue del aplicativo indicado en COB']        
                jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio  
            }
        
        }
        jiraTransitionIssue idOrKey: jiraKey, input: deployCob, site: sitio 
    }

    stage('Done'){
        echo '/*************Inicio de Metodo Done*************************/'
        commentJira =[body: 'Se ha realizado el CI/CD de forma correcta']        
        jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
        jiraTransitionIssue idOrKey: jiraKey, input: review, site: sitio
    }
}