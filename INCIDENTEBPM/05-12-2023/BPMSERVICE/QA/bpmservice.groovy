/****Inicio: variables Ambiente****/
def jiraKey
def pathJsonJira
def pathJsonBitBucket
def pathResponseBucket
def comandAnsible
def response
def responseJira
def responseToken
def responseBitBucket
def sitioweb
def pathBPMService
def responseAnsible
def ansibleResponse
def hostNameServer
def errorCode = 200
def msgErr
/****Fin: variables Ambiente****/


/****Inicio: variables Atlassian****/
def urlAtlassian
def sitio
def errorPreparing
def inBuilding 
def deploy 
def testing 
def review
def usuarioAtlassian
def usuarioBitBucket
def commentJira
def errorMesaje
/****Fin: variables Atlassian****/

/*****Inicio: UiPath*****/
def keyProject
def jsonnArrayRobots=[]
def upOrchestrator
def upFolder
def upTenan
def upAccount
/*****Fin: variables UiPath*****/

node('jenkins_slave01') {
    stage('Preparing'){
        echo '/*************Inicio de Metodo Preparing*************************/'
        
        /****Inicio: variables Ambiente****/
        jiraKey = idJira
        urlAtlassian = env.URL_REST_ATLASSIAN+jiraKey
        pathJsonJira = WORKSPACE+'/jsonTaskJira.json'
        pathJsonBitBucket = WORKSPACE+'/jsonTokenBitbucket.json'
        pathResponseBucket = WORKSPACE+'/jsonResposeBitbucket.json'
        usuarioAtlassian = 'atlassian'
        usuarioBitBucket = 'bitbucket-pull-request'      
        pathBPMService = env.PATH_QA_BPMSERVICE
        ansiblePath = env.ANSIBLE_BPMSERVICE
        comandAnsible = 'ansible-playbook '+ansiblePath
        /****Fin: variables Ambiente****/
        
        /****Inicio: variables Jira****/
        sitio=env.SITE_ATLASSIAN  
        inBuilding = [transition: [id: '181']]
        errorPreparing = [transition: [id: '221']]
        deploy = [transition: [id: '191' ]]
        errorBuild = [transition: [id: '231']]
        testing = [transition: [ id: '201']]
        errorDeploy = [transition: [id: '241']]
        errorTesting = [transition: [id: '251']]        
        review = [transition: [id: '211']]
        /****Fin: variables Jira****/
        
        /*****Inicio: UiPath*****/
        upOrchestrator = env.ORCHESTRATOR
        upFolder = env.FOLDER_QA
        upTenan = env.TENAN_UIPATH
        upAccount = env.ACCOUNT_UIPATH
        /*****Fin: variables UiPath*****/
        
        commentJira =[body: 'Se Inicio el ciclo CI/CD en Jenkins']        
        jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
        jiraTransitionIssue idOrKey: jiraKey, input: inBuilding, site: sitio 
        
    }
    
    stage('Building') {
        echo '/*************Inicio de Metodo Building*************************/'
        def codeResponse =300
        script{
            try{            
                response = httpRequest authentication: usuarioAtlassian ,url: urlAtlassian, outputFile: pathJsonJira
                codeResponse=response.status
            }catch(Exception){
                codeResponse = 300
            }
            
        }
        
        if ( codeResponse!= 200){
            commentJira =[body: 'Se encontro un error al consultar la tarea en la api, estatus code: '+response.status]        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio            
            jiraTransitionIssue idOrKey: jiraKey, input: errorBuild, site: sitio            
            sh "exit 1"            
        }else{
            script{
                try{
                    responseJira = readJSON file: pathJsonJira    
                    try{
                        build propagate: true, job: 'BITBUCKET_AURAPORTALDOC'
                        build propagate: true, job: 'BITBUCKET_AURAPORTALPROCESOS'
                        build propagate: true, job: 'BITBUCKET_BPMSERVICE'
                        build propagate: true, job: 'BITBUCKET_BPMSERVICEEAR'
                    }catch(Exception e){
                        errorMesaje = 'Error en la descarga del repositorio'
                        sh "exit 1"                         
                    }                
                    try{
                        hostNameServer = responseJira.fields.customfield_10054.value
                    }catch(Exception e){
                        errorMesaje = 'Debe de definir un servidor Valido'
                        sh "exit 1"                         
                    }
                    try{                    
                        jsonnArrayRobots = responseJira.fields.customfield_10049
                        jsonnArrayRobots.size()
                    }catch(Exception e){
                        jsonnArrayRobots=[]
                    }
                    try{
                        keyProject=responseJira.fields.project.key
                    }catch(Exception e){
                        keyProject=''
                    }
                   

                commentJira =[body: 'Se realizo la extracción de la información de la tarea, Se Procedera con el despliegue, esto puede demorar varios minutos']        
                jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                jiraTransitionIssue idOrKey: jiraKey, input: deploy, site: sitio

                }catch(Exception e){

                    if(errorMesaje==null){
                        errorMesaje=e.toString()
                    }
                    commentJira =[body: 'Ocurrio el siguiente error en la construcción '+errorMesaje]        
                    jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                    jiraTransitionIssue idOrKey: jiraKey, input: errorBuild, site: sitio
                    sh "exit 1" 
                }
            }
             
          
        }
    }

    stage('Deploy'){
        echo '/*************Inicio de Metodo Deploy*************************/'
        comandAnsible=comandAnsible+' -e env="uat" -e host='+ '"' + hostNameServer + '"' + ' -e id='+ '"' + jiraKey + '"' +' -e dir='+'"' + pathBPMService + '"'
        ansibleResponse=pathBPMService+'output.json'
        def comando = env.DELETE_SALTO_LINEA+' '+ansibleResponse
        
        script{
            try{
                sh "rm -f "+ ansibleResponse
                sh comandAnsible
            }catch(Exception){
                errorCode = 504
            }
            try{
                sh comando
                ansibleResponse = readJSON file: ansibleResponse
                msgErr= ansibleResponse.msg
            }catch(Exception){
                errorCode = 504
                msgErr= "ocurrio un error en el despliegue favor validar la construcción"
            }
        }
        
        
        if (errorCode!=200){
            commentJira =[body: 'Se encontro el siguiente error en el despliegue del aplicativo: '+msgErr]        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
            jiraTransitionIssue idOrKey: jiraKey, input: errorDeploy, site: sitio            
            sh "exit 1"

        }else{
            commentJira =[body: 'Se realizo el despliegue del aplicativo indicado']        
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
            jiraTransitionIssue idOrKey: jiraKey, input: testing, site: sitio  
        }
        
    }
    
    stage('Testing'){
        echo '*************Inicio de Metodo Testing*************************'
        script{ 
            try{
                errorCode = 200
                if (jsonnArrayRobots.size()>0){
                    commentJira =[body: 'Se procedera a la ejecucion de los robots indicados, esto puede demorar varios minutos']               
                    jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                }
                for (int i =0; i < jsonnArrayRobots.size(); i++){
                    if(errorCode==200){
                        upRobot = keyProject+"-"+jsonnArrayRobots[i].value
                        try{                   
                            build job: 'UIPATH', parameters: [
                                string(name: 'ROBOT', value: upRobot), 
                                string(name: 'ORCHESTRATOR', value: upOrchestrator),
                                string(name: 'FOLDER', value: upFolder),
                                string(name: 'TENAN', value: upTenan),
                                string(name: 'ACCOUNT', value: upAccount) ]
                            errorCode = 200 
                        }catch(Exception){
                            errorCode = 504
                            errorMesaje = "El robot "+upRobot +" no se pudo completar favor validar la configuracon del robot"
                            break
                        }
                    }             
                }

            }catch(Exception e){
                errorCode = 504
                errorMesaje = "Se encontro el siguiente error: "+e.toString()
            }
        }
        if (errorCode!=200){
            commentJira =[body: errorMesaje]               
            jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
            jiraTransitionIssue idOrKey: jiraKey, input: errorTesting, site: sitio   
            sh "exit 1"
        }
    }

   stage('Done'){
        echo '/*************Inicio de Metodo Done*************************/'
        script{
            try{
                commentJira =[body: 'Se ha Finalizado la implementación en el ambiente de QA']               
                jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio
                jiraTransitionIssue idOrKey: jiraKey, input: review, site: sitio
            }catch(Exception e){
                commentJira =[body: 'No es posible realizar la finalización en el ambiente de QA, Mensaje de error: '+e.toString()]               
                jiraAddComment input: commentJira, idOrKey: jiraKey, site: sitio      
                jiraTransitionIssue idOrKey: jiraKey, input: review, site: sitio           
                sh "exit 1"
            }
            
        }
    }
}