#!/usr/bin/ksh

export ORACLE_INSTANCE=/opt/forms/oracle/app/product/12.2.1/user_projects/domains/forms_domain/config/fmwconfig/components/FORMS/instances/forms1
export ORACLE_HOME=/opt/forms/oracle/app/product/12.2.1
export JAVA_HOME=/usr/java8_64
export TNS_ADMIN=/opt/forms/oracle/app/product/12.2.1/user_projects/domains/forms_domain/config/fmwconfig

export PATH=$ORACLE_INSTANCE/bin:$ORACLE_HOME/bin:/usr/local/bin:/bin:/usr/bin:/home/oracle/bin:$ORACLE_HOME/forms:/opt/acsel/libraries/pll:/opt/acsel/menu/mmb:/opt/acsel/forms/fmb:/opt/acsel/forms/bin:/opt/acsel/libraries/bin:/opt/acsel/menu/bin
export FORMS_PATH=$ORACLE_HOME/forms:$ORACLE_INSTANCE:/opt/acsel/libraries/pll:/opt/acsel/menu/mmb:/opt/acsel/forms/fmb:/opt/acsel/forms/bin:/opt/acsel/libraries/bin:/opt/acsel/menu/bin

export ORACLE_TERM=vt220
export TERM=vt220
export NLS_LANG=AMERICAN_AMERICA.WE8MSWIN1252


for i in `ls $1`
do
  echo Compiling library $i ...
  frmcmp_batch.sh userid=usuario/LaContraseña@SISADAN batch=yes module=$i module_type=library compile_all=yes window_state=minimize
done

